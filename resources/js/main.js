$(function() {

/* =============================================================================
  o 	MENU TRIGGERS
============================================================================= */

    $('#menu-trigger').on('click', function () {
        $('body').toggleClass('no-scroll');
        $('ul.menu').slideToggle();
    });

    $('.close-menu').on('click', function () {
        $('body').toggleClass('no-scroll');
        $('ul.menu').slideToggle();
    });


/* =============================================================================
   o   FOOTER MENU
============================================================================= */


    $(".menu-icon").click(function () {
        $(this).find('.fa').toggleClass("fa-bars fa-times");
        $( "ul.menu-footer" ).slideToggle();
    });


/* =============================================================================
  o   SLIDER
============================================================================= */
    if($(".bxslider").length){
        slider = $('.bxslider').bxSlider({
            nextText: '<i class="material-icons">arrow_forward</i>',
            prevText: '<i class="material-icons">arrow_back</i>',
            slideWidth: 260,
            minSlides: 1,
            maxSlides: 3,
            moveSlides: 1,
            slideMargin: 10,
            pager: false,
            responsive: true,
            infiniteLoop: true
        })

        var total_slide = slider.getSlideCount()

        if (total_slide < 2) {
            slider.destroySlider()
        }
    }


    if($(".full-slider").length){
        slider = $('.full-slider').bxSlider({
            nextText: '<i class="material-icons">arrow_forward</i>',
            prevText: '<i class="material-icons">arrow_back</i>',
            responsive: true,
            infiniteLoop: true
        })

        var total_slide = slider.getSlideCount()

        if (total_slide < 2) {
            slider.destroySlider()
        }
    }


/* =============================================================================
  o 	COUNTER
============================================================================= */
    var clock;

    $(document).ready(function() {
        var timespan = countdown(new Date(), new Date(2018, 4, 23),  countdown.SECONDS);

        var clock = $('.countdown').FlipClock(timespan.seconds, {
            countdown: true,
            clockFace: 'DailyCounter',
            showSeconds: false
        });
    });

}); 